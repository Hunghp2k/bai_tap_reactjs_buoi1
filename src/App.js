import logo from './logo.svg';
import './App.css';
import Navbar from './Bt_1/Navbar';
import Header from './Bt_1/Header';
import Item from './Bt_1/Item';
import Footer from './Bt_1/Footer';

function App() {
  return (
    <div className="App">
      <Navbar />
      <Header />
      <Item />
      <Footer />
    </div>
  );
}

export default App;
